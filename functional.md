---
title: Functional
---

### Kotlin and Functional Programming

---

```Kotlin
view.SetOnClickListener{ toast("Hello There") }
```

- this block `{ toast("Hello There") }` is called Lambda.

----

## Syntax of Lambda

```Kotlin
{x:Int, y:Int -> x + y}
```
- <div class="fragment" data-fragment-index="1">Always in curly braces</div>
- <div class="fragment" data-fragment-index="2">**x:Int, y:Int**: Parameters</div>
- <div class="fragment" data-fragment-index="3">**x+y**: Method body</div> 
- <div class="fragment" data-fragment-index="4">**->** separates parameters and body</div> 

----

#### Some other things to take care of

- Use {} in method body if more then one line
- If only one argument, you can omit variable name and use `it`
- Lambdas should be moved out of parenthesis if last argument

```Kotlin
filter ({ x:Int -> x > 0 })
// Can be rewritten as
filter ({ it > 0 })
// Move lambda out of parenthesis
filter { it > 0 }
```

---

### Returning from lambdas

- Last line of lambda becomes return statement by default.
- Return statement in lambda finishes outer functions.

```Kotlin
fun foo() {
    listOf(1, 2, 3, 4, 5).forEach {
        if (it == 3) return // this acts as return for `foo` directly
        println(it)
    }
    println("this point is unreachable")
}
```

----

### Qualified returns for lambdas

- With `return@{labelName}` we can return local function or return to any labels

```Kotlin
fun foo() {
    listOf(1, 2, 3, 4, 5).forEach {
        if (it == 3) return@forEach // this acts as return for forEach
        println(it)
    }
    println("this point is no longer unreachable")
}
```

----

### More complex example

```Kotlin
fun foo() {
    listOf(1, 2, 3, 4, 5).forEach first@{
        it.map second@
            { if(it%2!=0) return@second }//Returns just a map function
        if (it == 3) return@first // this acts as return for first
        println(it)
    }
    println("this point is no longer unreachable")
}
```

Note: Function names automatically becomes label names

---

## Functions as first class support

- Functions can be defined as normal variables.
- Just like normal variables they can be passed to & returned from methods

----

```Kotlin
data class Person(val name:String,val age:Int)

val people = getListOfPerson()

// Calls getter function
people.maxBy{ it.age }

// Calls getter function
people.maxBy(Person::age)
```

---

## Functions as variable.

- ({arguments})->{ReturnType}.
- **(Int,Int)->Int**: creates a function that asks two ints and returns other ints.
- Such functions are called first class functions 

----

```Kotlin
fun getDataStore(getOnlineFun: ()->Unit,
                     getOfflineFun: ()->Unit): ()->Unit {
        return if (isOnline && (!dataCache.isAvailable())) {
            getOnlineFun
        } else {
            getOfflineFun
        }

    }
```
Note: We may not need `Unit` when we writing functions, But we need it when passing functional arguments.


----

## Under the hood.

- Every function has a class type associated with it.
- <div class="fragment" data-fragment-index="1">`()->Boolean` = `Function0<Boolean>`.</div>
- <div class="fragment" data-fragment-index="2">`(Int,Int)->Int` = `Function2<Int,Int,Int>`.</div>
- <div class="fragment" data-fragment-index="3">`({args})->{Return Type}` = `Function{N}<{N number of arguments}, ReturnType>`.</div>

---

## Useful functionas.
#### With usecases

----

```Kotlin
data class Movie(val name:String, val directorName:String, val releaseYear:Int, val boxOfficeIncome: Long);

val movies = getListOfMovies();
```

----

#### 1. Find all movies directed by "Imtiaz Ali"

```Kotlin
movies.filter{ it.directorName == "Imtiaz Ali" }
```

----

#### 2. Find all movies released before 1975
#### And multiply 10000 in their income

```Kotlin
movies.filter { it.releaseYear <= 1975 }
        .map { it.boxOfficeIncome*10000 }
```

----

#### 3. Find all movies released in 2018 and find their average income

```Kotlin
movies.filter { it.releaseYear = 2018 }
        .map { it.boxOfficeIncome }
        .average()
```

----

#### 4. Find all multiword movies and print how many words are there

```Kotlin
movies.filter { it.name.contains(" ") }
        .map { it.name }
        .map { it.split(" ") }
        .map { it.length }
```

- Excercise: Find a shorter way

----

#### 5. Group all movies by their release year

```Kotlin
movies.groupBy { it.releaseYear}

// Will Output
{
    {GOTG 2, James Gunn, 2017},
    {Spiderman- Homecoming, Jon Watts, 2017},
    {Thor Ragnarok, Taika Waititi, 2017}
}
{
    {Black Panther, Ryan Coogler,2018}, 
    {Avengers-Infinity war, Russo Brothers,2018}, 
    {Ant man, Peyton Reed,2018}
}
```

Note: Talk about how grouping works and flatmap

----

#### 6. Get least earned steven speilberg movie.

```Kotlin
 movies.filter{ it.directorName = "Steven Spielberg" }
        .minBy{ it.boxOfficeIncome }
```

----

#### 7. Sort all Maniratnam movies by release year.

```Kotlin
 movies.filter{ it.directorName = "Maniratnam" }
            .sortedBy{ it.releaseYear }
```
