---
title: OOP
---

# Kotlin & OOP

---

- It's same as Java
- <div class="fragment" data-fragment-index="1">With some changes.</div>

---

# Visibility Modifiers

----

- In kotlin everything is `public` and `final` by default.
- We can have <div class="fragment" data-fragment-index="1"> `private` </div>  <div class="fragment" data-fragment-index="2"> & `internal` modifiers.</div> 

Note: Package Private in java: Visible only to classes visible in same package. While Internal is visible in same module. A module can be
an IntelliJ IDEA module;
a Maven project;
a Gradle source set;
a set of files compiled with one invocation of the Ant task.


----

- IN Kotlin every class is final by default. We can not extend classes.
- <div class="fragment" data-fragment-index="1">If we need to extend them, We need to have `open` keyword.</div>




---

Constructor

----

## Primary Consize constructor

```Kotlin
class Person(val name:String, val age:Int){

}
```

----

Is same as 

```Kotlin
class Person(name:String,age:Int){
    val name:String
    val age:Int
    //Init is actual constructor block
    init{
        this.name=name
        this.age=age
    }
}
```
Note: If constructor does not have any var/val. `init` is only block where we can access anything passed to constructor

----

## Secondary Constructor

```Kotlin
class Rectangle(val width:Int, val height:Int){
    //Above line is a primary constructor

    //Below line is Secondary constructor.
    constructor(side:Int) : this(side,side){

    }
}
```

----

## How to use constructor?

----

- Kotlin does not have `new` keyword.
- In above example. We can call like following.

```Kotlin
// Called with primary constructor
val actualRectangle = Rectangle(10,20)

// Called with secondary constructor
val square = Rectangle(30)
```

---

# Properties
### Not fields

----


- Property = Field + accessor(s)
- <div class="fragment" data-fragment-index="1">Read only Property = Field + getter</div>
- <div class="fragment" data-fragment-index="2">Mutable Property = Field + accessors</div>

Note: Accessors = getters+setters.
Mutable Property = property which can be changed

----

Properties can be read and written directly in Kotlin.

Consider following class.



```Kotlin
class Person(val name:String, var age:Int){

}
```

----

In kotlin, we can access and write to this as following

```Kotlin
val person = Person("Prasham",30)

//Prints Prasham
//Reading property
println(person.name)

//Writing to a property.
person.age=31
```

----

In Java the same code can be written like following

```Java
Person person = new Person("Prasham",30);

//Reading property
System.out.println(person.getName());

//Writing to property
person.setAge(31);
```

---

# Var vs Val

- <div class="fragment" data-fragment-index="1">Var = mutable property.</div>
- <div class="fragment" data-fragment-index="2">Val = Read only property.</div>

----

- In `Person` example. `name` is `val`, so it can't be changed.
- <div class="fragment" data-fragment-index="1">But `age` is `var`, so it can be changed easily.</div>

----

- Under the hood. 
- <div class="fragment" data-fragment-index="1">Every property has a `getter`.</div>
- <div class="fragment" data-fragment-index="2">But only `var` has a `setter`.</div>

----

## That leads us to our next point.

---

### Customized getter and setter

----

- Except data classes we can customize our logic in setter and getters.

----

- Like in `Rectangle` class. 

```Kotlin
class Rectangle(val width:Int, val height:Int){
    
    val isSquare: Boolean
        //Getter for isSquare
        get() {
            return width==height
        }
    
    constructor(side:Int) : this(side,side){

    }
}
```
Note: discuss about why isSquare is val and how we can change our logic in getter.

----

In the same `Rectangle` class

```Kotlin
class Rectangle(val width:Int, val height:Int){
    
    val isSquare: Boolean
        get() {
            //Getter for isSquare
            return width==height
        }
        
    var backgroundColor:Int
        set(value){
            // Setter for backgroundColor
            println("Setting BG Color $value")
            field = value
        }

    constructor(side:Int) : this(side,side){

    }
}
```

Note: Discuss why we should always write field = value

---

## Data class

- Class that holds value types together.

Note: Explain equality: Structural (Calling equals) and Refrential: Checking same reference

----

### Normal Class

```Kotlin
class Person(val name:String, val age:Int){

}
```
- They don't have `equals`, `hashCode` and `toString`.
- Why do we need them?

Note: Explain usage of each. Equals checks for equality and speedy index check. HashCode will make key-value based search easy and toString will give proper string representation.

----

### Making it a data class

- Just put `data` keyword before class.
- <div class="fragment" data-fragment-index="2">And remove `{}`.</div>

----

Like

```Kotlin
// Before
class Person(val name:String, val age:Int){

}

// After
data class Person(val name:String, val age:Int)
```

---

## Destructuring

- Extracting multiple values directly from object or array.
- Consider person class above.

----

```Kotlin
// Without destructoring
val person = Person("Prasham",30)
val name = person.name
val age = person.age

// With destructoring
val (name,age) = Person("Prasham",30);
```
- Both of these blocks are same

---

## Class hierarchy

#### A.K.A. How to extend something in Kotlin

----

- Every class inherits from `Any`. 
- We can only extend `open` or `abstract` classes.
- Syntax `class Derived:Base()`.
- When derived class has primary constructor, base class **must be initialized right there**.

Note: This is similar to calling `super` as first statement

----

## Overriding 

- No `@Override` annotation.
- Only `override fun`.
- <div class="fragment" data-fragment-index="1">Or `override val`. (Yes we can override values)</div>

----

### Curious case of interfaces.

- Unlike Java, interface can have their own logic.
- We can have 
    - <div class="fragment" data-fragment-index="1">fully qualified methods</div>
    - <div class="fragment" data-fragment-index="2">fully qualified values(with getters)</div>
    - <div class="fragment" data-fragment-index="3">abstract methods</div>
    - <div class="fragment" data-fragment-index="4">abstract vals</div>

----

For example

```Kotlin
interface CheckInterface{
    // Fully qualified val
    val answerToEveryQuestion:Int
        get()=42
    
    // Abstract val, classes have to override them
    val whatIsTheQuestionByTheWay:String

    // Abstract method
    fun doSomething()

    // Fully qualified method
    fun askQuestion(){
        println(whatIsTheQuestionByTheWay)
    }
}

```