---
title: Inline
---

## Inline Functions
- Inline functions: Functoinal body is replaced at call site.
- Android Method count limit.

Note: 65,536 methods
----

Without Inline.
```Kotlin
fun SharedPreferences.edit(
        func: SharedPreferences.Editor.() -> Unit) {
    val editor = edit()
    editor.func()
    editor.apply()
}

// Call site
preference.edit{
  putString("Test","Value")
}
```

----

With Inline

```Kotlin
inline fun SharedPreferences.edit(
        func: SharedPreferences.Editor.() -> Unit) {
    val editor = edit()
    editor.func()
    editor.apply()
}

// Call site
preference.edit{
  putString("Test","Value")
}

//Will be changed to
val editor = preference.edit()
putString("Test","Value")
editor.apply()
```

----

- You can have your cake an eat it too.
Note: You can still have your function, but it won't be counted against method count limit

---

### Crossinline & NoInline

- <div class="fragment" data-fragment-index="1">All lambdas passed in inline functions are inlined by default.</div>
- <div class="fragment" data-fragment-index="2">A return statement in inlined lambdas will finish main function, not the lambda. Called Non Local Returns.</div>

----

- Both will prevent non local returns in lambda arguments.
- <div class="fragment" data-fragment-index="1">`NoInline`: Passed lambdas will not be inlined, will be treated as fully qualified function.</div>
- <div class="fragment" data-fragment-index="2">`CrossInline`: Passed lambdas will be inlined, but returns will be limited to that lambda only.</div>

---

### Reified

- <div class="fragment" data-fragment-index="1">Sometimes we need type of parameter passed to us.</div>
- <div class="fragment" data-fragment-index="2">Without inline, we need reflection, which is costly and unreliable.</div>
- <div class="fragment" data-fragment-index="3">With Reified type we can easily access class information in our code.</div>

----

For Example

```Kotlin
inline fun <reified T> getMoshiAdapter(): JsonAdapter<T> = 
    Moshi.Builder().build().adapter<T>(T::class.java)
```

---

## Infix functions

- Functions marked with `infix` keywoard can omit `.` and `()` while passing arguments.
- Can work only if function recieves one argument.

```Kotlin
infix fun Any.to(other: String) = Pair.create(this,other)

"Christiano Ronaldo" to "Juventus"

infix fun String?.useIfEmpty(otherString: String?) = 
    if ((this ?: "").isEmptyString()) otherString ?:
         "" else (this ?: "")
         
val userName = name useIfEmpty "Nemo"
```