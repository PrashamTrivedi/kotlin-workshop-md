---
title: Intro
---

# Kotlin
## An Introduction


---


## Kotlin Programming language

- Modern.
- Pragmatic.
- Android-Friendly.


---

# Kotlin
### A Brief History

----

# Before time
## (2011-16)

- Started in **2011**, by *JetBrains*.
- For perspective **Swift was started in 2014**. 

----

## Large Adoption
### (2016-17)

- Went to first public beta around late 2015.
- Before that *JetBrains* was using kotlin internally.
- First stable version came in Feb 2016.
- Many developers picked this up.
- Jake Wharton's paper about Kotlin for square, made the community serious about kotlin.

## And Then

----

## Google Happened

- Google Adopted Kotlin as Official Language in I/O 2017.

<img data-src="https://j.gifs.com/1rAWvV.gif">
---

# Kotlin: Current

- Current Version: **1.2.51**.
- Apart from JVM, Kotlin Can be used in JavaScript.
- Google has used Kotlin to demo all Android code in I/O 2018.



----

## Kotlin Advantages

- Coming from *JetBrains*.
- Has one of the best tooling.
- Code Completion, Navigation, Refactoring & Stable inspections.

### And Above all

----

### Can be mixed with Java

----

### A bit more details

- Kotlin runs as a java library.
- A small runtime & some commands into build process.

---

# Some kotlin goodness

---

# Data Classes

----


```Kotlin
data class Money(val currency:Currency, val amount:Int);
```

---

# Extension functions

```Kotlin
fun String.capitalize() = toUpperCase();
```

will work as

```Kotlin
"barca".capitalize();
//Will output BARCA.
```

----

#### Around half of the kotlin library is Extension function.

Note: That makes kotlin standard library: AKA stdlib smaller and we can have less method count.

---

## First class functional support

----

- We can 
- <div class="fragment" data-fragment-index="1">Create functional variables</div>
- <div class="fragment" data-fragment-index="2">Pass functions into functions</div>
- <div class="fragment" data-fragment-index="3">Return a function from function</div>

----

For example We can write something like

```Kotlin
fun getDataStore(getOnlineFun: ()->Unit,
                     getOfflineFun: ()->Unit): ()->Unit {
        return if (isOnline && (!dataCache.isAvailable())) {
            getOnlineFun
        } else {
            getOfflineFun
        }

    }
```


