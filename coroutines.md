---
title: Coroutines
---

### Coroutines: From Beginner to advanced.

---

- We should not block main thread for long running tasks.
- <div class="fragment" data-fragment-index="1">Long running works should go in worker thread.</div>
- <div class="fragment" data-fragment-index="2">As parallely as possible.</div>
Note: Nearby events should not be dependent on User Profile

---

- Usecase
<div class="fragment" data-fragment-index="1">1. I have a token, I am sending it to server to verify it's validiy.</div>
<div class="fragment" data-fragment-index="2">2. Server gives me a user Id. I have to fetch User data using that.</div>
<div class="fragment" data-fragment-index="3">3. That user data has an image url, I have to load that Image in toolbar</div>
<div class="fragment" data-fragment-index="4">4. I have to load feeds in homescreen.</div>
<div class="fragment" data-fragment-index="5">5. All feeds have an image.</div>
<div class="fragment" data-fragment-index="6">Dependency. 1->2->3, 3 & 4 can run parallely. 5 are multiple parallel calls.</div>
<div class="fragment" data-fragment-index="7">
[(Pesudo)solution](#/4/5)
</div>
---

#### [Threading: Brief History of AndroidKind.... ](https://www.goodreads.com/book/show/23692271-sapiens)

----

- <div class="fragment" data-fragment-index="1">In prehistoric era, we used to have threads. Simple threads.</div>
- <div class="fragment" data-fragment-index="2">Then we moved to AsyncTask.</div>

----

#### AsyncTask: The Good, the bad & the Ugly

<div class="fragment" data-fragment-index="1">- **The Good**: A very robust and ahead of time API.</div>

<div class="fragment" data-fragment-index="2">- **The Bad**: Hard to maintain and Very leaky.</div>

<div class="fragment" data-fragment-index="3">- **The Ugly**: The knowledge</div>

<div class="fragment" data-fragment-index="4">
In short the community was not very happy around it, and started looking elsewhere.
</div>
----

#### RxJava: The Million dollar baby.

----

#### RxJava

- Provided good apis.
- Easy to switch threads.
- Parallelism & Sequential calls were never "that" easy before.
- There's an operator for that.
- Similar to Promise & Future APIs.
- Easy to prevent leaks

----

#### Callback hell- The confused programmer's inception
- Callback into Callback into Callback ...... and so on.
- No easy way of Exception propogation.
- Easy to get overwhelmed.

---

### Enter Coroutines

----
- Introduced in Kotlin 1.1.
- Works as `async-await` mechanism.
- No Callbacks, just normal methods.
- <div class="fragment" data-fragment-index="1">Just another<span class="fragment highlight-red" data-fragment-index="2"> smart and clean</span> way  to access another thread to offload work.</div>
- <div class="fragment" data-fragment-index="3">
Lot lighter then thread.
</div>

----


```Kotlin
GlobalScope.launch {
    val userData = getUserDataFromWebservice().await()
    showUserData(userData)
}

suspend fun getUserDataFromWebservice(){
    return async{
        // Do your retrofit work here
        return@ async wsResult
    }
}
``` 
----

### Building blocks

- Suspended function: A long running function e.g. w.s. call.
- A suspended function can only run in suspended function.
- <div class="fragment" data-fragment-index="1">`launch`: Start a coroutine and complete execution</div>
    <div class="fragment" data-fragment-index="2">- Returns `Job` object, useful to join executions and cancel</div>
- <div class="fragment" data-fragment-index="3">`async` : Starts a long running process in coroutine.</div>
    <div class="fragment" data-fragment-index="4">- Returns `Deferred<T>` object, calling `await()` will start long running process.</div>

----

### Solution of our problem using coroutines

- For Reference [our problem](#/2).

----

```Kotlin
GlobalScope.launch {
    //1
    val userId = validateTokenAndGetUserId(token).await() 
    val userData = getUserData(userId) //2
    val feeds = getFeeds(userId)   //3
    val user = userData.await()    //4
    showUserData(user)             //5
    showImage(user.imageUrl)       //6
    val feedData = feeds.await()   //4
    showFeeds(feedData)            //5
    for(feed in feedData){          //7
        showFeedImage(getFeedImages(feed.imageUrl).await()) 
        //8
    }
}
```

---

## Structured Concurrency

----

<div class="fragment" data-fragment-index="0">
- We can always have a parent child relation with Coroutines.
</div>
<div class="fragment" data-fragment-index="1">
- Before Coroutines `0.26`, We could call `launch` or `async` directly.
</div>
<div class="fragment" data-fragment-index="2">
- That had a problem, cancelling a parent doesn't always cancel a child, leads to a leak
</div>

----

- To solve this problem, starting v 0.26 Coroutines are Scoped.
- `GlobalScope` is one of them.
- We can Create our own scope.
- Demo time

----

#### Which block runs on which thread.

```Kotlin
GlobalScope.launch{
    //GlobalScope is provided in Library.
    //Has a lifecycle of Application
    //This will work in Default Dispatcher.
    //Will have two threads atleast
}
```

----

#### Custom Scope

```Kotlin
class Scope:CoroutineScope{
    ...
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    ...
    launch{
        //This will work in Main Thread
        async{
            //In same thread as parent by default
        }
    }
    ...
```

----
```Kotlin
class Scope:CoroutineScope{
    ...
    launch(Dispatchers.IO){
        //Will work in IO
        async(Dispatchers.Main){
            //Dangerous
            //Will work in main thread
        }
    }
    ...
}
```
---

### Shared Mutable State: The Problem

----

- Mutable: One that can change.
<div class="fragment">- Can create problems in programming.</div>
<div class="fragment">- Will definitely create problems in multi threaded programs.</div>
<div class="fragment">- Channels & Actors to the rescue.</div>
---

# CHANNELS

----

- Similar to blocking queue. 
- It can send and receive messages. 
<div class="fragment">- Both Send and Receive are suspending.</div>
<div class="fragment">- **Demo Time**</div>
<div class="fragment">- [Advanced Demo-Downloading a file and showing progress](https://gist.github.com/PrashamTrivedi/c7398c8f6bbf3d7cedb7919cd563302f)

---

# ACTORS

----

- Kind of multithreaded state machine.
- Can have State & Channel. 
- Process at most one message at a time.
- Mailbox type processing. FIFO queue.
- Suspends till current message is processed.
- Comparable with Java's thread pool which actor can manage.
- //TODO DEMO

---

### Misc

----

### Caveats

- Everything is sequential unless deferred.
- Recreation not possible, once a Job is finished or cancelled, it's gone forever.
- You can have only one coroutine to await send or receive per channel.
- Use `select` to process more than one channels in sigle coroutine. 
    - Using `onReceived` or `onSend`.

----

## Error Handling

- Just like normal `try/catch/finally` block.
- In `launch` error is thrown instantly.
- In `async` error is swallowed, thrown when we call `await`

----

## Error Handling - Advanced

- In parent-child relationship: First thrown exception is caught. 
    - All other exceptions are suppressed exception of first exception.
- `CancelationException` is special exception.
    - All other Exceptions are re-thrown, and will crash app.
    - `CancelationException` is not re-thrown, no logs will be printed.
    - Can be processed in a Catch block.

----
### Comparision with RxJava

- There is no backpressure. 
    - Channel suspends for next operation till current one finishes.
        - Sending suspends until someone starts receiving.
        - Receiving suspends until someone starts sending.
- `subscribeOn` is synonymous to `async`.
- `observer` is synonymous to `launch`.
- Not all. But many operators are already in Kotlin, with or without Coroutines.

---

## Study Material

- [Kotlin Coroutines-Intro (Talk from KotlinConf 2017)](https://www.youtube.com/watch?v=_hfBv0a09Jc)
- [How coroutines work- Deep dive into Coroutines on JVM](https://www.youtube.com/watch?v=YrrUCSi72E8)
- [Android Suspenders-KotlinConf 2018](https://www.youtube.com/watch?v=P7ov_r1JZ1g)
- [Kotlin Coroutine Guides - Everything Explained Easily with Examples](https://github.com/Kotlin/kotlinx.coroutines/blob/master/coroutines-guide.md)

---

<!-- .slide: data-background="http://images6.fanpop.com/image/photos/38700000/That-s-All-Folks-the-looney-tunes-show-38740983-1600-900.jpg" -->
