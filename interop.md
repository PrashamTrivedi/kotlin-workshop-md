---
title: Interop
---

## Interop

---

- Kotlin and Java can run along with each other.
- <div class="fragment" data-fragment-index="1">We can call Java code from Kotlin.</div>
- <div class="fragment" data-fragment-index="2">And (almost) run kotlin code from java.</div>
- <div class="fragment" data-fragment-index="3">This process is part of Interoperability.</div>

----

#### Calling Java code from Kotlin

- Just like calling other kotlin objects.
- Some keywords in Kotlin are valid methods in Java.
- Use backticks to call them. 
- E.g. `Mockito.when()`. Would be like.

```Kotlin
Mockito.`when`()
```

---

#### Calling Kotlin code from Java

- We have seen many examples before.
- Advanced Examples

---

### Files

- <div class="fragment" data-fragment-index="1">Functions can live without a class.</div>
- <div class="fragment" data-fragment-index="2">Java needs a Class, defaults to FileName.</div>
- <div class="fragment" data-fragment-index="3">`@file:JvmName` annotation.</div>

Note: Explain examples from kutils library. We will understand annotations later.

---

### Functions with default values.

- <div class="fragment" data-fragment-index="1">Java does not know about default values.</div>
- <div class="fragment" data-fragment-index="2">Fallback: overriding functions.</div>
- <div class="fragment" data-fragment-index="2">`@JvmOverloads` Annotation</div>

Note: Java hast to pass argument regardless of the value is default or not.

----

```Kotlin
 @JvmOverloads fun calcuateArea(width:Int, height: Int = width) = width*height
```

- Is same as

```Java
public int calculateArea(int width){
  return calculateArea(width,width);
}
public int calculateArea(int width,int height){
  return width*height;
}
```
---

### A word about static

- Kotlin doesn't have `Static`.
- <div class="fragment" data-fragment-index="1">It has Companion objects and methods.</div>
- <div class="fragment" data-fragment-index="2">Singletons are defined with `object SingleTonClass{}`.</div>
- <div class="fragment" data-fragment-index="3"> Const are automatically static for the class.</div>

----

```Kotlin
class MyClass {
    companion object {
        fun create(): MyClass = MyClass()
    }
}
val instance = MyClass.create()
```

----

- If we call this from Java, it gives us

```Java
  Myclass.Companion.create()
```

----

`@JvmStatic` to the rescue.


```Kotlin
class MyClass {
    companion object {
        @JvmStatic fun create(): MyClass = MyClass()
    }
}
val instance = MyClass.create()
```

----

- And In Java

```Java
  Myclass.create()
```

---

### Annotations in Kotlin

- `@file:JvmName("Funcs")`
- Format `@{Target}:{Annotation}({args})`.
- Target can be. `File`,`property`,`field`,`getter`,`setter`.
- E.g.
  ```Kotlin
  data class DbSchema(@field:Json(name = "formatVersion") val formatVersion: Int? = 0, @field:Json(
		name = "database") val database: Database? = Database())
  ```
