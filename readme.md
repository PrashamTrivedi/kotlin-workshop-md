# Kotlin Workshop material slides
This is kotlin workshop slides created using Markdown and powered by reveal.js, mostly reveal md

## Credits

- Full Credits To [Jetbains Kotlin workshop](https://github.com/JetBrains/kotlin-workshop), using whose material I have creted this slides.