---
title: DSL
---

## DSL

- Domain Specific Language.
- DSL vs GPL(General Purpose Language).
- An SQL Statement is a DSL.

Note: Java, Kotlin are GPL, we can write anything with it. DSL are useful to perform specific tasks. SELECT * from.....

----

- DSL is everywhere.
- Our gradle buildscript is a DSL.

```Kotlin
linearLayout(id="test",orientation=horizontal){
  ImageView(centerCrop=true,resource=R.drawable.wc_trophy)
  linearLayout(id="Names"){
      textView{
        text="France"
        drawableLeft=R.drawable.franceFlag
      }
      textView{
        text="Croatia"
        drawableLeft=R.drawable.croatFlag
      }
  }
}
```

----

### Real Life Example
- GlideDsl
```Kotlin
GlideDsl(groupItemViewModel.requestManager, it).apply {
                    url = viewModel.url //Original Url
                    thumbnailUrl =viewModel.thumb //Thumbnail Url
                    error(R.drawable.ic_placeholder_image) 
                    placeHolder(R.drawable.avd)
                    transform = false
                }.into(binding.imageView)
```

- Source [Here](https://gist.github.com/PrashamTrivedi/ec6e95a1c8ca947801cfb3bf89965d3e)