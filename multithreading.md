---
title: MultiThreading
---

### Asnycronous Calls in kotlin

---

- We should not block main thread for long running tasks.
- <div class="fragment" data-fragment-index="1">Long running works in worker thread.</div>
- <div class="fragment" data-fragment-index="2">As parallely as possible.</div>
Note: Nearby events should not be dependent on User Profile

---

### Calling worker thread

- Using AsyncTasks.
- <div class="fragment" data-fragment-index="1">Using RxJava.</div>
- <div class="fragment" data-fragment-index="2">Using Task api or other callbacks.</div>

----

### Problems

- AsyncTask: One return place for all, manual handling.
- RxJava - Task Api: Callbacks.

----

#### Callback Hell

- Everything is done in callback.
- Having multiple methods when handling something in callback.
- Propogating callback to other places: Callback in callback = Callback Hell...

---

## Coroutines

- Introduced in Kotlin 1.1.
- Works as `async-await` mechanism.
- No Callbacks, just normal methods.
- Sequential Flow


----

```Kotlin
launch(UI){
    val userData = getUserDataFromWebservice().await()
    showUserData(userData)
}

suspended fun getUserDataFromWebservice(){
    return async{
        // Do your retrofit work here
        return@ async wsResult
    }
}
``` 


----

- Coroutines is a language feature
- `launch`, `async` and `await` are library functions taking advantage of these functions.

----

### Threads vs Coroutines

- Threads are heavier object, co-routines are lightweight.
- <div class="fragment" data-fragment-index="1">You can't run 1,00,000 threads unless you have super-computer.</div>
- <div class="fragment" data-fragment-index="2">You can run 1,00,000 coroutines in your mobile.</div>
- <div class="fragment" data-fragment-index="3">Coroutines works on suspension, thread works on wait.</div>

Note: Coroutnes suspend a waiting call to let other call pass in same context. While Threads keeps their resources untill long running work is done.

---

### Building blocks

- Suspended function: A long running function e.g. w.s. call
- A suspended function can only run in suspended function.
- <div class="fragment" data-fragment-index="1">`launch`: Start a coroutine and complete execution</div>
    <div class="fragment" data-fragment-index="2">- Returns `Job` object, useful to join executions and cancel</div>
- <div class="fragment" data-fragment-index="3">`async` : Starts a long running process in coroutine.</div>
    <div class="fragment" data-fragment-index="4">- Returns `Deferred<T>` object, calling `await()` will start long running process.</div>

----

#### Which block runs where?

- `Context`: Thread context where the co-routines will work.
- <div class="fragment" data-fragment-index="1">Context of launch: Where result is being recieved.</div>
- <div class="fragment" data-fragment-index="2">Context of async: Where long running work is being done.</div>

---

- Excellent offical documentation available [here](https://github.com/Kotlin/kotlinx.coroutines/blob/master/coroutines-guide.md)
