---
title: Nulls
---

## Kotlin and Null pointers

---

<!-- .slide: data-background="https://www.azquotes.com/picture-quotes/quote-i-call-it-my-billion-dollar-mistake-it-was-the-invention-of-the-null-reference-in-1965-tony-hoare-113-96-46.jpg" -->

---

### Avoiding NullPointerException

- Make it compile time.
- <div class="fragment" data-fragment-index="1">Use `@Nullable` and `@NonNull` annotations to ensure compile time safety.</div>
- <div class="fragment" data-fragment-index="2">Check for nullability for runtime safety.</div>

---

- In Kotlin everything is `Non null` by default. 
- And we have to **Explicitly** mention nullability in Kotlin.

----

```Kotlin
// Non null by default
var first:String = "Test"

// Will throw compile time error
first = null // Not allowed.

// Nullable
var second:String? = null

second = "Another Test"
```

----

```Kotlin
println(first.length) //No problem as first is already non null
println(second.length) //Second can be null, compiler will complain
println(second?.length) //Now compiler is happy.
```

----

### `?` vs `!!`

- `?`: Null safe operator.
- in `second?.length`, `length` will only be called if `second` is not null.
- `second?.length` is equivalent to.
```Java
if(second!=null){
    second.length;
}
```

----

- `!!`: Double Bang operator, Or Non null assertion operator.
- Imagine this as two barrelled gun pointing at your own feet.
- For lovers of `NullPointerException`.
- in `second!!.length`. If `second` is null, app will throw `NullPointerException`.

----

- Avoid `!!` at all costs.
- It's easy to use `?`.
- In `person?.friend?.name?.toLowerCase()`, if any of `person`, `friend` or `name` is null, there won't be any exception.

----

### Checking for null conditions - 1.

- Normal `!= null` check.

```Kotlin
//Imagine name is String.

if(name!=null){
    // Now name is not null here. No need to call ? or !!
    println("length of Name is ${name.length}")
}
```

----

### Checking for null conditions - 2.

- Elvis operator A.K.A `?:`

<img data-src="https://dobsondev.com/wp-content/uploads/2014/06/elvis-operator.png">

Courtesy: dobsondev.com

----

```Kotlin
val a = person?.name?:"Nemo"
```

is equivalent to

```Kotlin
val a = if(person?.name!=null) person.name else "Nemo"
```

----


- A Non null variable must be initialized when being defined.
- Only Nullable variables (`?`) can be initialized later. 
- They will carry their `?` everywhere. 
- There needs to have a mechanism which allows a non null variable to be initialized later.

----

### LateInit

- `lateinit var myString:String`
- LateInit must be vars, they must be able to change values.
- Their initialization must be done as soon as possible.
- Accessing a `lateinit` property before it has been initialized throws a special exception.
- `this::myString.isInitialized` check to prevent this error.

----

### Typecasting and Nulls

- Safe Java: We check type using `instanceof`
- In Kotlin: We check type using `as`.

----

Java
```Java
AppUser user = (givenUserObject instanceof AppUser)? 
                    (AppUser) givenUserObject : null;
if(user!=null){
    System.out.println(user.getName());
}
```

Kotlin
```Kotlin
val user:AppUser? = if (givenUserObject as AppUser) 
                        givenUserObject else null

// Smart cast: In above case, in If block, 
// givenUserObject is already cast to AppUser object
println(user?.name)

// Or a shorter way, same as above
val user: AppUser? = givenUserObject as? AppUser
println(user?.name)
```

---

### Nullable types and Object Hierarchy

- `String` is NonNull type
- <div class="fragment" data-fragment-index="1">`String?` is Nullable type.</div>
- <div class="fragment" data-fragment-index="2"> Every Nullable type is supertype of NonNull type.</div>
- <div class="fragment" data-fragment-index="3">`String : String?`.</div>