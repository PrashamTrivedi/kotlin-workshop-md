---
title: Delegation
---

## Delegation

---

- Delegation: Transfering responsibility.
- <div class="fragment" data-fragment-index="1">In Kotlin: Transferring responsibility for creating object in various ways.</div>
- <div class="fragment" data-fragment-index="2">Using `by` keyword.</div>

Note: Explain delegation by real life example.

----

```Kotlin
class Example {
    var p: String by Delegate()
}
```

----

- Getters and setters under the hood.
- Way to share `get` and `set` logic between classes.

```Kotlin
class Delegate {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return "$thisRef, thank you for delegating '${property.name}' to me!"
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        println("$value has been assigned to '${property.name}' in $thisRef.")
    }
}
```

---

### Standard Delegates

----

### Lazy

- `val String by lazy{ "Test" }`
- <div class="fragment" data-fragment-index="1">Execute lambda only once, saves the result and returns same value afterwords.</div>
- <div class="fragment" data-fragment-index="2">Useful for set once, use everytime fields.</div>

----

#### Observable

- `val String by Delegates.Observable(default)){prop,new,old-> }`
- <div class="fragment" data-fragment-index="1">Default value is set at first.</div>
- <div class="fragment" data-fragment-index="1">Whenever a value is changed, lambda is called.</div>
- <div class="fragment" data-fragment-index="2">Useful to react to changes in variable.</div>

----

#### Vetoable

- Veto it: Reject any incoming proposal
- <div class="fragment" data-fragment-index="1">Almost same as observable.</div>
- <div class="fragment" data-fragment-index="2">Returns a boolean. If true returns value assigns to field.</div>

Note: Ask about UNSC members with veto, if mood is right

---

#### Creating custom delegate

- ReadOnlyProperty and ReadWriteProperty
- Override `getValue`, and use `setValue` if using ReadWriteProperty.
- Example of [Cusror delegate](https://gist.github.com/PrashamTrivedi/ed02e991a1340ee2d5fcaf38a7981658) 

